// Utils
import { delay } from '../../utils/delay';
// Store
import { cart } from './store';

export const getCartByIds = async (productIds) => {
  await delay(300);

  return cart.filter((e) => productIds.includes(e.product.id));
};

export const addToCart = async (productId, count) => {
  await delay(300);

  const isError = Math.random() < 0.2;
  if (isError) {
    throw "Unable to add item to cart";
  }

  let entry = cart.find((e) => e.product.id === productId);
  if (!entry) {
      entry = {
        product: {
            id: productId,
        },
        count: 0,
      };
      cart.push(entry);
  }
  entry.count += count;

  return {
      ok: true,
      count: entry.count,
  };
};

export const subFromCart = async (productId, count) => {
    await delay(300);
  
    const isError = Math.random() < 0.2;
    if (isError) {
      throw "Unable to sub item from cart";
    }
  
    let entry = cart.find((e) => e.product.id === productId);
    if (!entry) {
        throw "Item not found";
    }
    entry.count -= count;
  
    if (entry.count <= 0) {
      const id = cart.findIndex((e) => e.product.id === entry.productId);
      cart.splice(id, 1);
    }

    return {
        ok: true,
        count: entry.count,
    };
  };
