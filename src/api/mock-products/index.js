import { delay } from "../../utils/delay";
import { products } from './store';

/**
 * 
 * @param {string} query Search string for full-text search
 * @param {'title' | 'description'} queryField 
 * @param {{ [name: string]: any } | undefined} filters 
 * @param {number} offset
 */
export const queryProducts = async (query, queryField, filters, offset = 0) => {
  let res = products;
  let queryParts;
  if (query) {
    queryParts = query.replace(/[^A-Za-z0-9\s]/g, '').split(' ');
  }

  res = products.filter(
    (e) => {
      const filterPrice = filters?.price;
      if (filterPrice && (e.price < filterPrice.from || e.price > filterPrice.to)) {
        return false;
      }

      const filterGoldMillis = filters?.goldMillis;
      if (filterGoldMillis && (e.price < filterGoldMillis.from || e.price > filterGoldMillis.to)) {
        return false;
      }

      const filterSilverMillis = filters?.silverMillis;
      if (filterSilverMillis && (e.price < filterSilverMillis.from || e.price > filterSilverMillis.to)) {
        return false;
      }

      const filterCarats = filters?.silverCarats;
      if (filterCarats && (e.price < filterCarats.from || e.price > filterCarats.to)) {
        return false;
      }

      const filterWithPhoto = filters?.withPhoto;
      if (filterWithPhoto && !e.photoUrl) {
        return false;
      }

      if (queryParts) {
        const titleParts = e.title.replace(/[^A-Za-z0-9\s]/g, '').split(' ');
        const descriptionParts = e.description.replace(/[^A-Za-z0-9\s]/g, '').split(' ');
        const textMatch = queryField === 'title' ?
          titleParts.some((e1) => queryParts.some((e2) => {
            return e1.toLowerCase().startsWith(e2.toLowerCase());
          })) :
          descriptionParts.some((e1) => queryParts.some((e2) => {
            return e1.toLowerCase().startsWith(e2.toLowerCase());
          }));

        return textMatch;
      }

      return true;
    }
  );

  await delay(300);
  if (offset === 0) {
    const isError = Math.random() < 0.2;
    if (isError) {
      throw "Unable to fetch products";
    }
  }
  return {
    totalCount: res.length,
    items: res.slice(offset, offset + 3),
  };
}
