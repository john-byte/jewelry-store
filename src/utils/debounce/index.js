/**
 * @param {Function} fn Function to debounce
 * @param {number} ms Throttle milliseconds
 */
export const debounce = (fn, ms) => {
  let timer = null;
  return (...args) => {
    if (timer !== null) {
      clearTimeout(timer);
    }

    timer = setTimeout(
      () => {
        fn(...args);
        timer = null;
      },
      ms,
    );
  };
}