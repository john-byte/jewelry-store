import App from './App.svelte';

const root = document.getElementById('app');
const app = new App({
	target: root,
	hydrate: root.childNodes.length > 0,
});

export default app;